#!/bin/bash
export PYTHONPATH=/FairMOT/DCNv2/:/opt/ros/noetic/lib/python3/dist-packages:/FairMOT/src:/FairMOT/src/lib
source /opt/ros/noetic/setup.bash
#export ROS_MASTER_URI=http://10.68.0.1:11311
#roslaunch front_fisheye_from_basestation.launch  &
cd /FairMOT/src/
python3 demo_ros.py mot  --input-stream 1 --conf_thres $1 --vis $2 

