FROM nvcr.io/nvidia/cuda:11.0.3-cudnn8-devel-ubuntu20.04
RUN CUDA_VISIBLE_DEVICES=0
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update && apt-get install -y --no-install-recommends build-essential cmake git curl vim python3  python-dev python3-dev libssl-dev libffi-dev libxml2-dev libxslt1-dev zlib1g-dev python3-pip python3-pip locate pciutils python-setuptools python3-setuptools python3-opencv
RUN apt-get update && apt-get install -y lsb-release && apt-get clean all
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
RUN curl -sSL 'http://keyserver.ubuntu.com/pks/lookup?op=get&search=0xC1CF6E31E6BADE8868B172B4F42ED6FBAB17C654' | apt-key add -
RUN apt-get update && apt-get install -y -q apt-utils ros-noetic-desktop-full 
RUN apt-get install -y -q gstreamer1.0-plugins-ugly  gstreamer1.0-libav x11-apps 
RUN pip3 install --upgrade cython
RUN pip3 install -U cython_bbox  ipython
RUN pip3 install scipy numpy==1.19.5 matplotlib scikit-learn motmetrics lap yacs progress 
RUN pip3 install torch==1.7.1 torchvision==0.8.2 
RUN apt-get -y install python3-pip
RUN pip install torch torchvision future cython_bbox scipy
ADD FairMOT $HOME/FairMOT
RUN /FairMOT/DCNv2/make.sh && mv /_ext.cpython-38-x86_64-linux-gnu.so /FairMOT/DCNv2/
COPY front_fisheye_from_basestation.launch ./
COPY dla34-ba72cf86.pth /root/.cache/torch/hub/checkpoints/
COPY start_tracking.sh /
RUN apt-get -y install ros-noetic-ros-numpy