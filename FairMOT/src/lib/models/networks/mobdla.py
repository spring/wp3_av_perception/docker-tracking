from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import math
import logging
import numpy as np
from os.path import join

import torch
from torch import nn
import torch.nn.functional as F
import torch.utils.model_zoo as model_zoo
from typing import Callable, Any, Optional, List
from torch import Tensor

from dcn_v2 import DCN
from .utils import (
    round_filters,
    round_repeats,
    drop_connect,
    get_same_padding_conv2d,
    get_model_params,
    efficientnet_params,
    load_pretrained_weights,
    Swish,
    MemoryEfficientSwish,
    calculate_output_image_size
)

BN_MOMENTUM = 0.1
logger = logging.getLogger(__name__)

def get_model_url(data='imagenet', name='dla34', hash='ba72cf86'):
    return join('http://dl.yf.io/dla/models', data, '{}-{}.pth'.format(name, hash))

def conv3x3(in_planes, out_planes, stride=1):
    "3x3 convolution with padding"
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)

def _make_divisible(v: float, divisor: int, min_value: Optional[int] = None) -> int:
    """
    This function is taken from the original tf repo.
    It ensures that all layers have a channel number that is divisible by 8
    It can be seen here:
    https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet/mobilenet.py
    """
    if min_value is None:
        min_value = divisor
    new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
    # Make sure that round down does not go down by more than 10%.
    if new_v < 0.9 * v:
        new_v += divisor
    return new_v

class ConvBNActivation(nn.Sequential):
    def __init__(
        self,
        in_planes: int,
        out_planes: int,
        kernel_size: int = 3,
        stride: int = 1,
        groups: int = 1,
        norm_layer: Optional[Callable[..., nn.Module]] = None,
        activation_layer: Optional[Callable[..., nn.Module]] = None,
        dilation: int = 1,
    ) -> None:
        padding = (kernel_size - 1) // 2 * dilation
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        if activation_layer is None:
            activation_layer = nn.ReLU6
        super(ConvBNReLU, self).__init__(
            nn.Conv2d(in_planes, out_planes, kernel_size, stride, padding, dilation=dilation, groups=groups,
                      bias=False),
            norm_layer(out_planes),
            activation_layer(inplace=True)
        )
        self.out_channels = out_planes


# necessary for backwards compatibility
ConvBNReLU = ConvBNActivation

class InvertedResidual(nn.Module):
    def __init__(
        self,
        inp: int,
        oup: int,
        stride: int,
        expand_ratio: int,
        norm_layer: Optional[Callable[..., nn.Module]] = None
    ) -> None:
        super(InvertedResidual, self).__init__()
        self.stride = stride
        assert stride in [1, 2]

        if norm_layer is None:
            norm_layer = nn.BatchNorm2d

        hidden_dim = int(round(inp * expand_ratio))
        self.use_res_connect = self.stride == 1 and inp == oup

        layers: List[nn.Module] = []
        if expand_ratio != 1:
            # pw
            layers.append(ConvBNReLU(inp, hidden_dim, kernel_size=1, norm_layer=norm_layer))
        layers.extend([
            # dw
            ConvBNReLU(hidden_dim, hidden_dim, stride=stride, groups=hidden_dim, norm_layer=norm_layer),
            # pw-linear
            nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
            norm_layer(oup),
        ])
        self.conv = nn.Sequential(*layers)
        self.out_channels = oup
        self._is_cn = stride > 1

    def forward(self, x: Tensor) -> Tensor:
        if self.use_res_connect:
            return x + self.conv(x)
        else:
            return self.conv(x)


class Root(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, residual):
        super(Root, self).__init__()
        self.conv = nn.Conv2d(
            in_channels, out_channels, 1,
            stride=1, bias=False, padding=(kernel_size - 1) // 2)
        self.bn = nn.BatchNorm2d(out_channels, momentum=BN_MOMENTUM)
        self.relu = nn.ReLU(inplace=True)
        self.residual = residual

    def forward(self, *x):
        children = x
        x = self.conv(torch.cat(x, 1))
        x = self.bn(x)
        if self.residual:
            x += children[0]
        x = self.relu(x)

        return x

class Tree(nn.Module):
    def __init__(self, levels, block, in_channels, out_channels, stride, t, norm_layer, root_dim=0, level_root=False,
                root_kernel_size=1,
                root_residual=False ):
        super(Tree, self).__init__()
        if root_dim == 0:
            root_dim = 2 * out_channels
        if level_root:
            root_dim += in_channels
        if levels == 1:
            self.tree1 = block(in_channels, out_channels, stride, expand_ratio=t, norm_layer=norm_layer)
            stride = 1  # spatial dimentions don't change
            in_channels = out_channels  # keep number of channels
            self.tree2 = block(in_channels, out_channels, stride, expand_ratio=t, norm_layer=norm_layer)
        else:
            self.tree1 = Tree(levels - 1, block, in_channels, out_channels, stride, t, norm_layer,
                              root_dim=0,
                              root_kernel_size=root_kernel_size,
                              root_residual=root_residual)
            stride = 1  # spatial dimenstions don't change
            in_channels = out_channels  # keep number of channels
            self.tree2 = Tree(levels - 1, block, in_channels, out_channels, stride, t, norm_layer,
                              root_dim=root_dim + out_channels,
                              root_kernel_size=root_kernel_size,
                              root_residual=root_residual)

        if levels == 1:
            self.root = Root(root_dim, out_channels, root_kernel_size, root_residual)
        self.level_root = level_root
        self.root_dim = root_dim
        self.downsample = None
        self.project = None
        self.levels = levels
        if stride > 1:
            self.downsample = nn.MaxPool2d(stride, stride=stride)
        if in_channels != out_channels:
            self.project = nn.Sequential(
                nn.Conv2d(in_channels, out_channels,
                          kernel_size=1, stride=1, bias=False),
                nn.BatchNorm2d(out_channels, momentum=BN_MOMENTUM)
            )

    def forward(self, x, residual=None, children=None):
        children = [] if children is None else children
        bottom = self.downsample(x) if self.downsample else x
        residual = self.project(bottom) if self.project else bottom
        if self.level_root:
            children.append(bottom)
        # x1 = self.tree1(x, residual)
        x1 = self.tree1(x)
        if self.levels == 1:
            x2 = self.tree2(x1)
            x = self.root(x2, x1, *children)
        else:
            children.append(x1)
            x = self.tree2(x1, children=children)
        return x

class DLA(nn.Module):
    def __init__(self, channels, num_classes=1000,
                 width_mult: float = 1.0,
                 round_nearest: int = 8,
                 block=InvertedResidual, residual_root=False, linear_root=False,
                 inverted_residual_setting: Optional[List[List[int]]] = None,
                 norm_layer: Optional[Callable[..., nn.Module]] = None):
        super(DLA, self).__init__()

        self.channels = channels
        self.num_classes = num_classes

        if block is None:
            block = InvertedResidual

        if norm_layer is None:
            norm_layer = nn.BatchNorm2d

        input_channel = 32
        last_channel = 1280

        if inverted_residual_setting is None:
            inverted_residual_setting = [
                # t, c, n, s
                [1, 16, 1, 1],
                [6, 24, 2, 2],
                [6, 32, 2, 2],  # [6, 32, 3, 2],
                [6, 64, 4, 2],
                [6, 96, 4, 1],  # [6, 96, 3, 1],
                [6, 160, 2, 2],  # [6, 160, 3, 2],
                [6, 320, 1, 1],
            ]
        # Batch norm parameters
        bn_mom = 0.01
        bn_eps = 0.001

        # Get stem static or dynamic convolution depending on image size
        image_size = 224
        Conv2d = get_same_padding_conv2d(image_size=image_size)

        # Stem
        in_channels = 3  # rgb
        out_channels = 32
        self._conv_stem = Conv2d(in_channels, out_channels, kernel_size=3, stride=2, bias=False)
        self._bn0 = nn.BatchNorm2d(num_features=out_channels, momentum=bn_mom, eps=bn_eps)
        self.base_layer = nn.Sequential(self._conv_stem, self._bn0, nn.ReLU(inplace=True))

        idx = 0
        for t, c, n, s in inverted_residual_setting:
            output_channel = _make_divisible(c * width_mult, round_nearest)
            if n < 2:
                level = block(input_channel, output_channel, s, expand_ratio=t, norm_layer=norm_layer)
                input_channel = output_channel
            else:
                level = Tree(n // 2, block, input_channel, output_channel, s, t, norm_layer)
                input_channel = output_channel

            # setattr(self, 'level{}'.format(idx), level)
            setattr(self, 'level' + str(idx), level)
            idx += 1
            # print(t, c, n, s, sum(p.numel() for p in level.parameters() if p.requires_grad))

    def forward(self, x):
        y = []
        x = self.base_layer(x)
        for i in range(0, 7):
            x = getattr(self, 'level{}'.format(i))(x)
            y.append(x)
        return y


    def _make_level(self, block, inplanes, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or inplanes != planes:
            downsample = nn.Sequential(
                nn.MaxPool2d(stride, stride=stride),
                nn.Conv2d(inplanes, planes,
                          kernel_size=1, stride=1, bias=False),
                nn.BatchNorm2d(planes, momentum=BN_MOMENTUM),
            )

        layers = []
        layers.append(block(inplanes, planes, stride, downsample=downsample))
        for i in range(1, blocks):
            layers.append(block(inplanes, planes))

        return nn.Sequential(*layers)

    def _make_conv_level(self, inplanes, planes, convs, stride=1, dilation=1):
        modules = []
        for i in range(convs):
            modules.extend([
                nn.Conv2d(inplanes, planes, kernel_size=3,
                          stride=stride if i == 0 else 1,
                          padding=dilation, bias=False, dilation=dilation),
                nn.BatchNorm2d(planes, momentum=BN_MOMENTUM),
                nn.ReLU(inplace=True)])
            inplanes = planes
        return nn.Sequential(*modules)

    def load_pretrained_model(self, data='imagenet', name='dla34', hash='ba72cf86'):
        # fc = self.fc
        if name.endswith('.pth'):
            model_weights = torch.load(data + name)
        else:
            model_url = get_model_url(data, name, hash)
            model_weights = model_zoo.load_url(model_url)
        num_classes = len(model_weights[list(model_weights.keys())[-1]])
        self.fc = nn.Conv2d(
            self.channels[-1], num_classes,
            kernel_size=1, stride=1, padding=0, bias=True)
        self.load_state_dict(model_weights)
        # self.fc = fc

def mobdla(**kwargs):
    model = DLA([32, 16, 24, 32, 64, 96, 160, 320], block=InvertedResidual, **kwargs)
    return model

class Identity(nn.Module):

    def __init__(self):
        super(Identity, self).__init__()

    def forward(self, x):
        return x

def fill_fc_weights(layers):
    for m in layers.modules():
        if isinstance(m, nn.Conv2d):
            if m.bias is not None:
                nn.init.constant_(m.bias, 0)

def fill_up_weights(up):
    w = up.weight.data
    f = math.ceil(w.size(2) / 2)
    c = (2 * f - 1 - f % 2) / (2. * f)
    for i in range(w.size(2)):
        for j in range(w.size(3)):
            w[0, 0, i, j] = \
                (1 - math.fabs(i / f - c)) * (1 - math.fabs(j / f - c))
    for c in range(1, w.size(0)):
        w[c, 0, :, :] = w[0, 0, :, :]

class DeformConv(nn.Module):
    def __init__(self, chi, cho):
        super(DeformConv, self).__init__()
        self.actf = nn.Sequential(
            nn.BatchNorm2d(cho, momentum=BN_MOMENTUM),
            nn.ReLU(inplace=True)
        )
        # self.conv = DCN(chi, cho, kernel_size=(3, 3), stride=1, padding=1, dilation=1, deformable_groups=1)
        self.conv = nn.Conv2d(chi, cho, kernel_size=3, stride=1, padding=1, bias=False, dilation=1)


    def forward(self, x):
        x = self.conv(x)
        x = self.actf(x)
        return x

class IDAUp(nn.Module):

    def __init__(self, o, channels, up_f):
        super(IDAUp, self).__init__()
        for i in range(1, len(channels)):
            c = channels[i]
            f = int(up_f[i])
            proj = DeformConv(c, o)
            node = DeformConv(o, o)
            if f == 1:
                #  deal with no change in spatial dimensions by using forward convolution
                up = nn.Conv2d(o, o, 3, padding=1)
            else:
                up = nn.ConvTranspose2d(o, o, f * 2, stride=f, padding=f // 2, output_padding=0, groups=o, bias=False)
            fill_up_weights(up)

            setattr(self, 'proj_' + str(i), proj)
            setattr(self, 'up_' + str(i), up)
            setattr(self, 'node_' + str(i), node)

    def forward(self, layers, startp, endp):
        # for i in range(startp + 1, endp):
        for i in range(startp + 1, endp):
            upsample = getattr(self, 'up_' + str(i - startp))
            project = getattr(self, 'proj_' + str(i - startp))
            layers[i] = upsample(project(layers[i]))
            node = getattr(self, 'node_' + str(i - startp))
            layers[i] = node(layers[i] + layers[i - 1])

class DLAUp(nn.Module):
    def __init__(self, startp, channels, scales, in_channels=None):
        super(DLAUp, self).__init__()
        self.startp = startp
        if in_channels is None:
            in_channels = channels
        self.channels = channels
        channels = list(channels)
        scales = np.array(scales, dtype=int)
        for i in range(len(channels) - 1):
            j = -i - 2
            setattr(self, 'ida_{}'.format(i), IDAUp(channels[j], in_channels[j:], scales[j:] // scales[j]))
            scales[j + 1:] = scales[j]
            in_channels[j + 1:] = [channels[j] for _ in channels[j + 1:]]

    def forward(self, layers):
        out = [layers[-1]]
        # for i in range(len(layers) - self.startp - 1):
        for i in range(len(layers) - self.startp):
            ida = getattr(self, 'ida_{}'.format(i))
            ida(layers, len(layers) - i - 2, len(layers))
            out.insert(0, layers[-1])
        return out

class Interpolate(nn.Module):
    def __init__(self, scale, mode):
        super(Interpolate, self).__init__()
        self.scale = scale
        self.mode = mode

    def forward(self, x):
        x = F.interpolate(x, scale_factor=self.scale, mode=self.mode, align_corners=False)
        return x

class DLASeg(nn.Module):
    def __init__(self, base_name, heads, pretrained, down_ratio, final_kernel,
                 last_level, head_conv, out_channel=0):
        super(DLASeg, self).__init__()
        # assert down_ratio in [2, 4, 8, 16]

        self.first_level = 1  # int(np.log2(down_ratio))
        self.last_level = last_level
        self.base = globals()[base_name]()  # calls eff0
        channels = self.base.channels

        #  DLA output to heads
        Conv2d = get_same_padding_conv2d(image_size=224)
        self._final_conv = Conv2d(16, 64, kernel_size=3, stride=2, bias=False)

        # scales = [2 ** i for i in range(len(channels[self.first_level:]))]
        scales = [ 1, 2, 4, 8, 8, 16, 16]
        # scales = [1, 2, 4, 8, 8, 16]

        if out_channel == 0:
            out_channel = channels[self.first_level]

        # self.ida_up = IDAUp(out_channel, channels[self.first_level:self.last_level],
        #                     [2 ** i for i in range(self.last_level - self.first_level)])

        self.dla_up = DLAUp(self.first_level, channels[self.first_level:], scales)

        self.ida_up = IDAUp(channels[self.first_level], channels[self.first_level:self.last_level], [ 1, 2, 4, 8, 8, 16 ])
        # self.ida_up = IDAUp(out_channel, channels[self.first_level:self.last_level], [1, 1, 2, 4, 8, 8])
        # self.ida_up = IDAUp(out_channel, channels[self.first_level:self.last_level], [1, 2, 4, 8, 8])

        self.heads = heads
        for head in self.heads:
            classes = self.heads[head]
            if head_conv > 0:
                fc = nn.Sequential(
                    # nn.Conv2d(channels[self.first_level], head_conv, kernel_size=3, padding=1, bias=True),
                    nn.Conv2d(64, head_conv, kernel_size=3, padding=1, bias=True),
                    nn.ReLU(inplace=True),
                    nn.Conv2d(head_conv, classes, kernel_size=final_kernel, stride=1, padding=final_kernel // 2, bias=True))
                if 'hm' in head:
                    fc[-1].bias.data.fill_(-2.19)
                else:
                    fill_fc_weights(fc)
            else:
                # fc = nn.Conv2d(channels[self.first_level], classes, kernel_size=final_kernel, stride=1, padding=final_kernel // 2, bias=True)
                fc = nn.Conv2d(64, classes, kernel_size=final_kernel, stride=1, padding=final_kernel // 2, bias=True)
                if 'hm' in head:
                    fc.bias.data.fill_(-2.19)
                else:
                    fill_fc_weights(fc)
            self.__setattr__(head, fc)

    def forward(self, x):
        #
        # #  scale image according to image_size in GlobalParams and divisible by 64
        # sfactor = self._global_params.image_size / min(x.shape[-2:])
        # target_size = np.asarray([x.shape[-2:][0], x.shape[-2:][1]])
        # target_size = target_size * sfactor
        # target_size = target_size.astype(int)
        # # if target_size[0] != target_size[1]:
        # #     adj = target_size != (2 * self._global_params.image_size)  # select the one that is different to 2*GlobalParams
        # #     target_size[adj] = target_size[adj][0] - targConvBNActivationet_size[adj][0] % 64  # make it divisible by 64
        # target_size[0] = target_size[0] - target_size[0] % 32  # make it divisible by 64
        # target_size[1] = target_size[1] - target_size[1] % 32  # make it divisible by 64
        # target_size = list(target_size)
        # x = F.interpolate(x, size=target_size, mode='bilinear')

        x = self.base(x)
        x = self.dla_up(x)

        y = []
        for i in range(self.last_level - self.first_level):
            y.append(x[i].clone())
        self.ida_up(y, 0, len(y))

        y = self._final_conv(y[-1])  # convert to 64 channels and image_size/4 for head input

        z = {}
        for head in self.heads:
            # z[head] = self.__getattr__(head)(y[-1])
            z[head] = self.__getattr__(head)(y)
        return [z]

def get_pose_net(heads, head_conv=256, down_ratio=4):

    model = DLASeg('mobdla', heads,
                   pretrained=False,
                   down_ratio=down_ratio,
                   final_kernel=1,
                   last_level=7,
                   head_conv=head_conv)
    return model
