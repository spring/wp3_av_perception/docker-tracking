import cv2
import numpy as np
import scipy
import lap
from scipy.spatial.distance import cdist

from cython_bbox import bbox_overlaps as bbox_ious
from lib.tracking_utils import kalman_filter

import time

def merge_matches(m1, m2, shape):
    O,P,Q = shape
    m1 = np.asarray(m1)
    m2 = np.asarray(m2)

    M1 = scipy.sparse.coo_matrix((np.ones(len(m1)), (m1[:, 0], m1[:, 1])), shape=(O, P))
    M2 = scipy.sparse.coo_matrix((np.ones(len(m2)), (m2[:, 0], m2[:, 1])), shape=(P, Q))

    mask = M1*M2
    match = mask.nonzero()
    match = list(zip(match[0], match[1]))
    unmatched_O = tuple(set(range(O)) - set([i for i, j in match]))
    unmatched_Q = tuple(set(range(Q)) - set([j for i, j in match]))

    return match, unmatched_O, unmatched_Q


def _indices_to_matches(cost_matrix, indices, thresh):
    matched_cost = cost_matrix[tuple(zip(*indices))]
    matched_mask = (matched_cost <= thresh)

    matches = indices[matched_mask]
    unmatched_a = tuple(set(range(cost_matrix.shape[0])) - set(matches[:, 0]))
    unmatched_b = tuple(set(range(cost_matrix.shape[1])) - set(matches[:, 1]))

    return matches, unmatched_a, unmatched_b


def linear_assignment(cost_matrix, thresh):
    if cost_matrix.size == 0:
        return np.empty((0, 2), dtype=int), tuple(range(cost_matrix.shape[0])), tuple(range(cost_matrix.shape[1]))
    matches, unmatched_a, unmatched_b = [], [], []
    cost, x, y = lap.lapjv(cost_matrix, extend_cost=True, cost_limit=thresh)
    # print(cost)
    for ix, mx in enumerate(x):
        if mx >= 0:
            matches.append([ix, mx])
    unmatched_a = np.where(x < 0)[0]
    unmatched_b = np.where(y < 0)[0]
    matches = np.asarray(matches)
    return matches, unmatched_a, unmatched_b


def ious(atlbrs, btlbrs):
    """
    Compute cost based on IoU
    :type atlbrs: list[tlbr] | np.ndarray
    :type atlbrs: list[tlbr] | np.ndarray

    :rtype ious np.ndarray
    """
    ious = np.zeros((len(atlbrs), len(btlbrs)), dtype=np.float)
    if ious.size == 0:
        return ious

    ious = bbox_ious(
        np.ascontiguousarray(atlbrs, dtype=np.float),
        np.ascontiguousarray(btlbrs, dtype=np.float)
    )

    return ious


def iou_distance(atracks, btracks):
    """
    Compute cost based on IoU
    :type atracks: list[STrack]
    :type btracks: list[STrack]

    :rtype cost_matrix np.ndarray
    """

    if (len(atracks)>0 and isinstance(atracks[0], np.ndarray)) or (len(btracks) > 0 and isinstance(btracks[0], np.ndarray)):
        atlbrs = atracks
        btlbrs = btracks
    else:
        atlbrs = [track.tlbr for track in atracks]
        btlbrs = [track.tlbr for track in btracks]
    _ious = ious(atlbrs, btlbrs)
    cost_matrix = 1 - _ious

    return cost_matrix

def embedding_distance(tracks, detections, metric='cosine'):
    """
    :param tracks: list[STrack]
    :param detections: list[BaseTrack]
    :param metric:
    :return: cost_matrix np.ndarray
    """

    cost_matrix = np.zeros((len(tracks), len(detections)), dtype=np.float)
    if cost_matrix.size == 0:
        return cost_matrix, None
    det_features = np.asarray([track.curr_feat for track in detections], dtype=np.float)
    #for i, track in enumerate(tracks):
        #cost_matrix[i, :] = np.maximum(0.0, cdist(track.smooth_feat.reshape(1,-1), det_features, metric))
    track_features = np.asarray([track.smooth_feat for track in tracks], dtype=np.float)
    cost_matrix = np.maximum(0.0, cdist(track_features, det_features, metric))  # Nomalized features
    return cost_matrix, det_features

# def embedding_distance_to_stored(tracks, cost_matrix, det_features, acc_features=None, metric='cosine'):
#     """ Calculates the distance between detected boxes and past features
#     :param tracks: list[STrack]
#     :param detections: list[BaseTrack]
#     :param metric:
#     :return: cost_matrix np.ndarray
#     """
#
#     if cost_matrix.size == 0:
#         return cost_matrix
#     # det_features = np.asarray([track.curr_feat for track in detections], dtype=np.float)
#     #for i, track in enumerate(tracks):
#         #cost_matrix[i, :] = np.maximum(0.0, cdist(track.smooth_feat.reshape(1,-1), det_features, metric))
#
#     for i, track in enumerate(tracks):
#         # if there are stored features (from the beginning to 10 seconds ago
#         if np.sum(acc_features[:-10, track.track_id, :]) != 0:
#
#             # find indices with non-zero stored historic data
#             data_idx = np.asarray(np.where(np.sum(acc_features[:-10, track.track_id, :], axis=1) != 0))[0]
#             hfeat = acc_features[data_idx, track.track_id, :]  # store the historic features
#
#             # # join current features with stored past ones
#             # hfeat = np.concatenate((acc_features[data_idx, track.track_id, :], track.smooth_feat.reshape(1,-1)), axis=0)
#
#             # get the shortest distances to historic data
#             hist_min = np.min(np.maximum(0.0, cdist(hfeat, det_features, metric)), axis=0)
#
#             # get the shortest distances to most recent features
#             current_min = np.min(np.maximum(0.0, cdist(track.smooth_feat.reshape(1,-1), det_features, metric)), axis=0)
#
#             # if hist_min < current_min:
#             # cost_matrix[i, :] = current_min
#                 # true_idx = np.where(hist_min < current_min)
#             true_idx = np.where(hist_min < 0.05)
#             cost_matrix[i, true_idx] = hist_min[true_idx]
#             # else:
#             # cost_matrix[i, :] = current_min
#             #     cost_matrix[i, :] = hist_min
#             # cost_matrix[i, :] = np.min(np.maximum(0.0, cdist(hfeat, det_features, metric)), axis=0)
#             # cost_matrix[i, :] = np.mean(np.maximum(0.0, cdist(hfeat, det_features, metric)), axis=0)
#         # else:
#         #     # hist_min = np.min(np.maximum(0.0, cdist(track.smooth_feat.reshape(1,-1), det_features, metric)), axis=0)
#         #     # if hist_min < 0.2:
#         #     #     cost_matrix[i, :] = hist_min
#         #     cost_matrix[i, :] = np.maximum(0.0, cdist(track.smooth_feat.reshape(1,-1), det_features, metric))
#         #     # cost_matrix[i, :] = np.mean(np.maximum(0.0, cdist(track.smooth_feat.reshape(1,-1), det_features, metric)), axis=0)
#
#         # print(track, cost_matrix[i, :])
#         # cost_matrix[i, :] = np.maximum(0.0, cdist(track.smooth_feat.reshape(1,-1), det_features, metric))
# #    track_features = np.asarray([track.smooth_feat for track in tracks], dtype=np.float)
# #    cost_matrix = np.maximum(0.0, cdist(track_features, det_features, metric))  # Nomalized features
#     return cost_matrix

def embedding_distance_to_stored(tracks, cost_matrix, det_features, acc_features=None, metric='cosine'):
    """ Calculates the distance between detected boxes and past features
    :param tracks: list[STrack]
    :param detections: list[BaseTrack]
    :param metric:
    :return: cost_matrix np.ndarray
    """

    if cost_matrix.size == 0:
        return cost_matrix

    for i, track in enumerate(tracks):
        # if there are stored features (from the beginning to 5 seconds ago
        if np.sum(acc_features[:-5, track.track_id, :]) != 0:

            # find indices with non-zero stored historic data
            data_idx = np.asarray(np.where(np.sum(acc_features[:-5, track.track_id, :], axis=1) != 0))[0]
            hfeat = acc_features[data_idx, track.track_id, :]  # store the historic features

            # get the shortest distances to historic data
            hist_min = np.min(np.maximum(0.0, cdist(hfeat, det_features, metric)), axis=0)

            # get the shortest distances to most recent features
            current_min = np.min(np.maximum(0.0, cdist(track.smooth_feat.reshape(1,-1), det_features, metric)), axis=0)

            true_idx = np.where(hist_min < 0.05)
            # true_idx = np.where( (current_min - hist_min > 0))
            # cost_matrix[i, true_idx] = hist_min[true_idx]
            cost_matrix[i, true_idx] = 1E-6

    cost_matrix[cost_matrix != np.min(cost_matrix, axis=0)] = 1E6
    return cost_matrix


def check_reid_distance(tracks, cost_matrix, det_features, acc_features, thresh=0.5, metric='cosine'):
    """ Checks the distance between detected boxes and the tracklets to avoid reassigning to previous id in case
    distances are consistently too large
    :param tracks: list[STrack]
    :param detections: list[BaseTrack]
    :param acc_features: historic data for features
    :param thresh: distance threshold to reject reidentification of lost track
    :param metric:
    :return: cost_matrix np.ndarray
    """

    if cost_matrix.size == 0:
        return cost_matrix

    for i, track in enumerate(tracks):
        # if track.end_frame - track.start_frame < 125:  # 5 seconds
            if np.sum(acc_features[:-5, track.track_id, :]) != 0:  # check only up tp 3 seconds earlier

                # find for which time indices there are non-zero stored historic data
                data_idx = np.asarray(np.where(np.sum(acc_features[:-5, track.track_id, :], axis=1) != 0))[0]

                # find for which time indices there are non-zero stored historic data
                hfeat = acc_features[data_idx, track.track_id, :]

                # cost_matrix[i, np.min(np.maximum(0.0, cdist(hfeat, det_features, metric)), axis=0) > thresh] = np.inf
                cost_matrix[i, np.min(np.maximum(0.0, cdist(hfeat, det_features, metric)), axis=0) > thresh] = np.inf

    return cost_matrix


def gate_cost_matrix(kf, cost_matrix, tracks, detections, only_position=False):
    if cost_matrix.size == 0:
        return cost_matrix
    gating_dim = 2 if only_position else 4
    gating_threshold = kalman_filter.chi2inv95[gating_dim]
    measurements = np.asarray([det.to_xyah() for det in detections])
    for row, track in enumerate(tracks):
        gating_distance = kf.gating_distance(
            track.mean, track.covariance, measurements, only_position)
        cost_matrix[row, gating_distance > gating_threshold] = np.inf
    return cost_matrix


def fuse_motion(kf, cost_matrix, tracks, detections, only_position=False, lambda_=0.98):
    """Increases the cost as the tracklet when detected box gets farther apart
       Lambda controls how much the cost is increased.
    """
    if cost_matrix.size == 0:
        return cost_matrix
    gating_dim = 2 if only_position else 4
    gating_threshold = kalman_filter.chi2inv95[gating_dim]
    measurements = np.asarray([det.to_xyah() for det in detections])
    for row, track in enumerate(tracks):
        # if track.state != 1:
        #     continue
        # measure distance from tracklet to detected box
        gating_distance = kf.gating_distance(
            track.mean, track.covariance, measurements, only_position, metric='maha')
        # cost_matrix[row, gating_distance > gating_threshold] = np.inf  # if distance is too large, do not link
        cost_matrix[row] = lambda_ * cost_matrix[row] + (1 - lambda_) * gating_distance
    return cost_matrix
