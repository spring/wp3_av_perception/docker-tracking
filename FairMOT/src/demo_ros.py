from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
import os
import os.path as osp
from lib.opts import opts
from lib.tracking_utils.utils import mkdir_if_missing
from lib.tracking_utils.log import logger
import lib.datasets.dataset.jde as datasets
from track import eval_stream

logger.setLevel(logging.INFO)

def demo(opt):
    if int(opt.input_stream) == True:
        print("input = stream")

        result_root = opt.output_root if opt.output_root != '' else '.'
        print("result_root = ", result_root)

        mkdir_if_missing(result_root)

        logger.info('Starting tracking...')
        result_filename = os.path.join(result_root, 'stream.txt')

        frame_dir = None if opt.output_format == 'text' else osp.join(result_root, 'stream')
        eval_stream(opt, 'mot', result_filename, save_dir=frame_dir, show_image=True)

    elif int(opt.sim_egomotion) == True:

        # opt.input_video ='../videos/MOT16-03.mp4'   # override the input video
        # opt.input_video ='../videos/MOT17-11-SDP.avi'   # override the input video
        opt.img_size = (1088, 608)                  # override  image size into the model: (1088, 608), (864, 480), (576, 320), (288, 160), (160, 96)

        result_root = opt.output_root if opt.output_root != '' else '.'
        mkdir_if_missing(result_root)

        logger.info('Starting tracking...')
        dataloader = datasets.LoadVideo(opt.input_video, opt.img_size)
        result_filename = os.path.join(result_root, 'results.txt')
        frame_rate = dataloader.frame_rate

        frame_dir = None if opt.output_format == 'text' else osp.join(result_root, 'frame')
        eval_ego_patch(opt, dataloader, 'mot', result_filename,
                 save_dir=frame_dir, show_image=True, frame_rate=frame_rate)

        if opt.output_format == 'video':
            output_video_path = osp.join(result_root, 'results.mp4')
            cmd_str = 'ffmpeg -f image2 -i {}/%05d.jpg -b 5000k -c:v mpeg4 {}'.format(osp.join(result_root, 'frame'),
                                                                                      output_video_path)
            os.system(cmd_str)
    else:
        result_root = opt.output_root if opt.output_root != '' else '.'
        mkdir_if_missing(result_root)

        logger.info('Starting tracking...')
        dataloader = datasets.LoadVideo(opt.input_video, opt.img_size)
        result_filename = os.path.join(result_root, 'results.txt')
        frame_rate = dataloader.frame_rate

        frame_dir = None if opt.output_format == 'text' else osp.join(result_root, 'frame')
        eval_seq(opt, dataloader, 'mot', result_filename,
                 save_dir=frame_dir, show_image=True, frame_rate=frame_rate)
                 # save_dir=None, show_image=True, frame_rate=frame_rate)

        if opt.output_format == 'video':
            output_video_path = osp.join(result_root, 'output.mp4')
            cmd_str = 'ffmpeg -f image2 -i {}/%05d.jpg -b 5000k -c:v mpeg4 {}'.format(osp.join(result_root, 'frame'),
                                                                                      output_video_path)
            os.system(cmd_str)

if __name__ == '__main__':
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    opt = opts().init()
    demo(opt)
