from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import os.path as osp
import cv2
import logging
import argparse
import motmetrics as mm
import numpy as np
import torch
import subprocess
import time
import rospy
import time
import sys
import lib.datasets.dataset.jde as datasets
from sensor_msgs.msg import CompressedImage, Image, RegionOfInterest
from lib.tracker.multitracker import JDETracker
from lib.tracking_utils import visualization as vis
from lib.tracking_utils.log import logger
from lib.tracking_utils.timer import Timer
from lib.tracking_utils.evaluation import Evaluator
from lib.tracking_utils.utils import mkdir_if_missing
from lib.opts import opts
from lib.datasets.dataset.jde import letterbox
# from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import String
import ros_numpy

def eval_stream(opt, data_type, result_filename, save_dir=None, show_image=True):
    # w0, h0 = 352, 224
    # w0, h0 = 512, 384
    w0, h0 = 640, 480
    # w0, h0 = 704, 448
    # w0, h0 = 768, 576
    # w0, h0 = 1024, 768
    # w0, h0 = 1280, 960
    # w0, h0 = 3264, 2448  # original fisheye front
    img_res = [w0, h0]

    # depth calibration based on bounding box height and a person 1.84m tall. Referred to an image size of 640x480
    # model: y = m / x + b, where x is the depth
    # therefore, x = m / (y - b)
    m = 336.4 * h0/480   # standing
    b = 15.9 * h0/480    # standing
    sr = 2.25                   # min standing height to width ratio threshold
    ms = 199.6 * h0/480  # sitting
    bs = 20.5  * h0/480  # sitting
    depth_param = [m,b,sr,ms,bs]
    # os.environ['ROS_MASTER_URI'] = 'http://localhost:11311'
    # os.environ['ROS_MASTER_URI'] = 'http://192.168.0.10:11311'
    #os.environ['ROS_MASTER_URI'] = 'http://10.68.0.1:11311'
    os.environ['PATH'] += os.pathsep + "/opt/ros/noetic/lib"
#    os.environ["LD_LIBRARY_PATH"] +=  "/opt/ros/noetic/lib"
    sys.path.append("/opt/ros/noetic/lib")


    frame_rate = 30
    if save_dir:
        mkdir_if_missing(save_dir)
    tracker = JDETracker(opt, frame_rate=frame_rate)
    timer = Timer()
    results = []
    frame_id = 0

    # start retrieving and processing images for tracking
    ic = GetImageFromTopic(tracker, opt, img_res, depth_param)
    rospy.init_node('tracker', anonymous=False)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print ("Shutting down ROS Image feature detector module")
    cv2.destroyAllWindows()

class GetImageFromTopic:

    def __init__(self, tracker, opt, ir, dp):
        '''Initialize ros , ros subscriber'''

        if os.path.split(opt.img_topic)[1] == "image_raw":
            img_type = Image
        elif os.path.split(opt.img_topic)[1] == "compressed":
            img_type = CompressedImage
        else:
            print("ERROR in topic name: Check topic name and make sure it doesn't have '/' at the end")
            exit(1)

        self.subscriber = rospy.Subscriber(opt.img_topic, img_type, self.callback,  queue_size = 1)
        self.pub = rospy.Publisher('/tracker/tracker_output', String, queue_size=10)
        self.opt = opt
        # self.bridge = CvBridge()
        self.w0, self.h0 = ir[0], ir[1]
        self.tracker = tracker
        self.frame_id = 0
        self.results = []
        self.show_image = int(opt.vis)
        self.m = dp[0]
        self.b = dp[1]
        self.sr = dp[2]
        self.ms = dp[3]
        self.bs = dp[4]
        self.timer = Timer()
        self.xy_scale = None
	#self.last_time = 0

    def callback(self, data):
        '''Callback function of subscribed topic.
        Here images get converted and features detected'''

	#current = rospy.get_time()

        # rospy.loginfo('Image received...')
        try:
          # cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
          cv_image = np.frombuffer(data.data, np.uint8)
          cv_image = cv2.imdecode(cv_image, cv2.IMREAD_COLOR)
          img0 = cv2.resize(cv_image, (self.w0, self.h0))
          self.xy_scale = [cv_image.shape[1]/self.w0, cv_image.shape[0]/self.h0]
          img, _, _, _ = letterbox(img0, height=self.h0, width=self.w0)

          # Normalize RGB
          img = img[:, :, ::-1].transpose(2, 0, 1)
          img = np.ascontiguousarray(img, dtype=np.float32)
          img /= 255.0

          # run tracking
          self.timer.tic()
          blob = torch.from_numpy(img).cuda().unsqueeze(0)
          online_targets = self.tracker.update(blob, img0)
          online_tlwhs = []
          online_ids = []
          #online_scores = []
          for t in online_targets:
              tlwh = t.tlwh
              tlwh[0] *= self.xy_scale[0]
              tlwh[1] *= self.xy_scale[1]
              tlwh[2] *= self.xy_scale[0]
              tlwh[3] *= self.xy_scale[1]
              tid = t.track_id
              vertical = tlwh[2] / tlwh[3] > 1.6
              if tlwh[2] * tlwh[3] > self.opt.min_box_area and not vertical:
                  online_tlwhs.append(tlwh)
                  online_ids.append(tid)
                  #online_scores.append(t.score)
          self.timer.toc()
          track_res = str(time.time()) + "," + str(self.frame_id + 1)
          if len(online_ids) > 0:
              for i in range(len(online_ids)):
                  #  watch for negative offsets
                  online_tlwhs[i][0] = max(0, online_tlwhs[i][0])
                  online_tlwhs[i][1] = max(0, online_tlwhs[i][1])

                  # publish ROIs
                  self.roi_pub = rospy.Publisher('/humans/bodies/' + "{0:0=4d}/".format(online_ids[i]) + 'roi',
                                                 RegionOfInterest, queue_size=10)
                  try:
                      ROI = RegionOfInterest()
                      ROI.x_offset = int(online_tlwhs[i][0])
                      ROI.y_offset = int(online_tlwhs[i][1])
                      ROI.width = int(online_tlwhs[i][2])
                      ROI.height = int(online_tlwhs[i][3])
                      self.roi_pub.publish(ROI)
                  except:
                      rospy.loginfo("Publishing ROI failed")

                  # publish cropped bonding boxes
                  self.crop_pub = rospy.Publisher('/humans/bodies/' + "{0:0=4d}/".format(online_ids[i]) + 'cropped',
                                                 Image, queue_size=10)
                  try:
                      #  crop bounding box
                      crop = cv_image[int(online_tlwhs[i][1]):int(online_tlwhs[i][1])+int(online_tlwhs[i][3]),
                                      int(online_tlwhs[i][0]):int(online_tlwhs[i][0])+int(online_tlwhs[i][2])]
                      # convert crop array to ROS Image message
                      crop_msg = ros_numpy.msgify(Image, crop, encoding='rgb8')
                      self.crop_pub.publish(crop_msg)
                      # cv2.imshow('crop', cv2.cvtColor(crop, cv2.COLOR_BGR2BGRA))
                      # k = cv2.waitKey(1)
                  except:
                      rospy.loginfo("Publishing cropped bounding box failed")

                  if online_tlwhs[i][3] / online_tlwhs[i][2] > self.sr:  # if height-width ratio above sitting threshold
                      track_res = track_res + "," + str(online_ids[i]) + " " + str(online_tlwhs[i])[1:-1] + " " \
                              + str(np.round(self.m / (online_tlwhs[i][3] - self.b), 2))
                  else:  # if sitting
                       track_res = track_res + "," + str(online_ids[i]) + " " + str(online_tlwhs[i])[1:-1] + " " \
                              + str(np.round(self.ms / (online_tlwhs[i][3] - self.bs), 2))
          # p_in.stdin.write(str(list((frame_id + 1, online_tlwhs, online_ids))) + "\n")
          track_res_str = track_res + "\n"

          # print(str(list((frame_id + 1, online_tlwhs, online_ids))))
          print(track_res_str)

          # publish tracking results to /tracker/tracker_output
          self.pub.publish(track_res_str)


          # save results
          #self.results.append((self.frame_id + 1, online_tlwhs, online_ids))

          if self.show_image: # or save_dir is not None:
              # online_im = vis.plot_tracking(img0, online_tlwhs, online_ids, frame_id=self.frame_id, fps=1. / self.timer.average_time)
              online_im = vis.plot_tracking(cv_image, online_tlwhs, online_ids, frame_id=self.frame_id, fps=1. / self.timer.average_time)
          if self.show_image:
              if online_im.shape[1] > 1280:
                  width = 1280
                  height = 960
                  cv2.imshow('online_im', cv2.resize(online_im, (width, height), cv2.INTER_CUBIC))
              else:
                cv2.imshow('online_im', online_im)
              k = cv2.waitKey(1)

          self.frame_id += 1
          # print(frame_id)
          #rospy.loginfo(f"image FPS: {1/(current - self.last_time)}")
          #self.last_time = current

        # except CvBridgeError as e:
        except ValueError  as e:
          print(e)


if __name__ == '__main__':
    os.environ['CUDA_VISIBLE_DEVICES'] = '1'
    opt = opts().init()


    if not opt.val_mot16:
        seqs_str = '''KITTI-13
                      KITTI-17
                      ADL-Rundle-6
                      PETS09-S2L1
                      TUD-Campus
                      TUD-Stadtmitte'''
        #seqs_str = '''TUD-Campus'''
        data_root = os.path.join(opt.data_dir, 'MOT15/images/train')
    else:
        seqs_str = '''MOT16-02
                      MOT16-04
                      MOT16-05
                      MOT16-09
                      MOT16-10
                      MOT16-11
                      MOT16-13'''
        data_root = os.path.join(opt.data_dir, 'images')
    if opt.test_crowdhuman_office:
        seqs_str = '''val'''
        data_root = os.path.join(opt.data_dir, 'crowdhuman_office/images')
    if opt.test_mot16:
        seqs_str = '''MOT16-01
                      MOT16-03
                      MOT16-06
                      MOT16-07
                      MOT16-08
                      MOT16-12
                      MOT16-14'''
        #seqs_str = '''MOT16-01 MOT16-07 MOT16-12 MOT16-14'''
        #seqs_str = '''MOT16-06 MOT16-08'''
        data_root = os.path.join(opt.data_dir, 'MOT16/test')
    if opt.test_mot15:
        seqs_str = '''ADL-Rundle-1
                      ADL-Rundle-3
                      AVG-TownCentre
                      ETH-Crossing
                      ETH-Jelmoli
                      ETH-Linthescher
                      KITTI-16
                      KITTI-19
                      PETS09-S2L2
                      TUD-Crossing
                      Venice-1'''
        data_root = os.path.join(opt.data_dir, 'MOT15/images/train')
    if opt.test_mot17:
        seqs_str = '''MOT17-04-SDP
                      MOT17-05-SDP
                      MOT17-09-SDP
                      MOT17-10-SDP
                      MOT17-11-SDP
                      MOT17-13-SDP'''
        # seqs_str = '''MOT17-04-SDP'''
        #seqs_str = '''MOT17-01-SDP MOT17-07-SDP MOT17-12-SDP MOT17-14-SDP'''
        #seqs_str = '''MOT17-03-SDP'''
        #seqs_str = '''MOT17-06-SDP MOT17-08-SDP'''
        data_root = os.path.join(opt.data_dir, 'MOT17/images/train')
    if opt.val_mot17:
        seqs_str = '''MOT17-02-SDP
                      MOT17-04-SDP
                      MOT17-05-SDP
                      MOT17-09-SDP
                      MOT17-10-SDP
                      MOT17-11-SDP
                      MOT17-13-SDP'''
        #seqs_str = '''MOT17-02-SDP'''
        data_root = os.path.join(opt.data_dir, 'MOT17/images/test')
    if opt.test_jrdb:
        seqs_str = '''gt_image_0
                      gt_image_2
                      gt_image_4
                      gt_image_6
                      gt_image_8  '''

        # seqs_str = '''gt_image_0'''
        # indoors
        data_root = os.path.join(opt.data_dir, 'jrdb/images/test/gates-ai-lab-2019-02-08_0')
        # data_root = os.path.join(opt.data_dir, 'jrdb/images/test/huang-2-2019-01-25_0')
        # data_root = os.path.join(opt.data_dir, 'jrdb/images/test/nvidia-aud-2019-04-18_0')
        # data_root = os.path.join(opt.data_dir, 'jrdb/images/test/tressider-2019-04-26_2')

        # outdoors
        # data_root = os.path.join(opt.data_dir, 'jrdb/images/train/clark-center-intersection-2019-02-28_0')
        # data_root = os.path.join(opt.data_dir, 'jrdb/images/test/meyer-green-2019-03-16_1')
        # data_root = os.path.join(opt.data_dir, 'jrdb/images/test/tressider-2019-03-16_1')



    if opt.val_mot15:
        seqs_str = '''Venice-2
                      KITTI-13
                      KITTI-17
                      ETH-Bahnhof
                      ETH-Sunnyday
                      PETS09-S2L1
                      TUD-Campus
                      TUD-Stadtmitte
                      ADL-Rundle-6
                      ADL-Rundle-8
                      ETH-Pedcross2
                      TUD-Stadtmitte'''
        #seqs_str = '''Venice-2'''
        data_root = os.path.join(opt.data_dir, 'MOT15/images/train')
    if opt.val_mot20:
        seqs_str = '''MOT20-01
                      MOT20-02
                      MOT20-03
                      MOT20-05
                      '''
        data_root = os.path.join(opt.data_dir, 'MOT20/images/train')
    if opt.test_mot20:
        seqs_str = '''MOT20-04
                      MOT20-06
                      MOT20-07
                      MOT20-08
                      '''
        data_root = os.path.join(opt.data_dir, 'MOT20/images/test')
    seqs = [seq.strip() for seq in seqs_str.split()]

    main(opt,
         data_root=data_root,
         seqs=seqs,
         exp_name='MOT17_test_public_dla34',
         show_image=True,
         save_images=False,
         save_videos=False)
