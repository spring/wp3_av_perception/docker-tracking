#!/usr/bin/env bash
rm /FairMOT/DCNv2/*.so
rm -r /FairMOT/DCNv2/build/
python3 /FairMOT/DCNv2/setup.py build develop
