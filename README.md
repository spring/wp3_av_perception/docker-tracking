This repository contains the necessary instructions to run or alternatively to create a Docker image that allows ARI to track people in the scene using the front fisheye camera. It processes  images that have been streamed from ARI and republished on a host computer (basestation). The republishing is carried out with the help of an image republishing docker. To start republishing from ARI on your own basestation run the following comands (you may have to login with Gitlab user name and password):
1. `docker login registry.gitlab.inria.fr` 
2. `docker pull registry.gitlab.inria.fr/spring/dockers/wp3_republish_on_basestation`
3. `docker run  -it --rm --env ROS_MASTER_URI=http://10.68.0.1:11311 --net=host -e DISPLAY=${DISPLAY}  -v /tmp/.X11-unix:/tmp/.X11-unix --gpus all  registry.gitlab.inria.fr/spring/dockers/wp3_republish_on_basestation  bash -c "./start_republish.sh"`

This should make available rgb images from the front head, fisheye and torso cameras in ARI. Only the front fisheye is used, in principle, for tracking.

To test the tracking docker run the following command:
`docker run --rm  -it --env ROS_MASTER_URI=http://10.68.0.1:11311 --net=host -e DISPLAY=${DISPLAY}  -v /tmp/.X11-unix:/tmp/.X11-unix --gpus all  registry.gitlab.inria.fr/spring/dockers/wp3_tracker   bash -c "./start_tracking.sh 0.5 1"`
The two values at the end of the above command are, respectively, the detection threshold for tracking and a tag for visualizing (1) or not visualizing (0) the tracking on a window.

Alternatively, if you wish to build and test the docker image for tracking, follow the steps below (the default runtime must be first set to nvidia. There is done in steps 1-3, also explained at https://stackoverflow.com/questions/59691207/docker-build-with-nvidia-runtime):

1. Install nvidia-container-runtime:

    `sudo apt-get install nvidia-container-runtime`

2. Edit/create the /etc/docker/daemon.json with content:

```
{
    "runtimes": {
        "nvidia": {
            "path": "/usr/bin/nvidia-container-runtime",
            "runtimeArgs": []
         } 
    },
    "default-runtime": "nvidia" 
}
```

3. Restart docker daemon:

    `sudo systemctl restart docker`

4. Download and unzip this repository to your machine, enter the created  directory and execute:

    `docker build -t <image_name> .`

Then, you can test the Docker image with the following commands:

    `xhost +`

    `docker run  -it --rm --env ROS_MASTER_URI=http://10.68.0.1:11311 --net=host -e DISPLAY=${DISPLAY}  -v /tmp/.X11-unix:/tmp/.X11-unix --gpus all  <image_name> bash -c "./start_tracking.sh 0.5 1"`

If running the docker remotely with ssh, make sure you use the -X option. Then the following commands:
```
XSOCK=/tmp/.X11-unix
XAUTH=`echo $(mktemp).xauth`
touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -
docker run -it --rm --env ROS_MASTER_URI=http://10.68.0.1:11311 -v $XSOCK:$XSOCK:rw -v $XAUTH:$XAUTH:rw -e XAUTHORITY=${XAUTH} -e DISPLAY=$DISPLAY --net=host --gpus all \
<image_name> bash -c "./start_tracking.sh 0.5 1"
```



Make also sure you are connected to **ARI** in your Wifi-Networks settings.

The result of the tracking is published to the topic "/tracker/tracker_output". At the moment it consists of a string with commas separating information of each identified person. For each person, returned values are bounding box position, its dimensions and finally the estimated distance of the person from the camera. 



